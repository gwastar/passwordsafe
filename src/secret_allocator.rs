use std::{
    fmt::Debug,
    ptr::{null_mut, NonNull},
};

#[allow(unused_imports)]
use std::collections::{BTreeSet, HashSet};

use libc::{
    c_void, madvise, mlock, munlock, munmap, MADV_DONTDUMP, MADV_DONTNEED, MAP_ANONYMOUS,
    MAP_FAILED, MAP_PRIVATE, PROT_READ, PROT_WRITE,
};
use zeroize::Zeroize;

const MIN_ORDER: usize = 3;
const MIN_SIZE: usize = 1 << MIN_ORDER;

const MAX_ORDER: usize = 14;
pub const MAX_SIZE: usize = 1 << MAX_ORDER;

const PAGE_ORDER: usize = 12;
const PAGE_SIZE: usize = 1 << PAGE_ORDER;

// type SetType = HashSet<*mut u8>;
type SetType = BTreeSet<*mut u8>;

#[derive(Debug)]
pub struct SecretAllocator {
    free_lists: [SetType; MAX_ORDER - MIN_ORDER + 1],
    num_regions: usize,
}

impl SecretAllocator {
    pub fn new() -> Self {
        Self {
            free_lists: Default::default(),
            num_regions: 0,
        }
    }

    fn get_free_list(&mut self, order: usize) -> &mut SetType {
        debug_assert!((MIN_ORDER..=MAX_ORDER).contains(&order));
        &mut self.free_lists[order - MIN_ORDER]
    }

    fn allocate_new_region(&mut self) -> *mut u8 {
        let mut map_size = 2 * MAX_SIZE;
        let ptr = unsafe {
            libc::mmap(
                null_mut(),
                map_size,
                PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS,
                -1,
                0,
            )
        };
        if ptr == MAP_FAILED {
            panic!("mmap failed {}", std::io::Error::last_os_error());
        }

        let offset = (MAX_SIZE - (ptr as usize & (MAX_SIZE - 1))) & (MAX_SIZE - 1);
        assert!(offset % PAGE_SIZE == 0);
        if offset != 0 && unsafe { munmap(ptr, offset) } != 0 {
            panic!("munmap failed {}", std::io::Error::last_os_error());
        }
        let ptr = unsafe { ptr.add(offset) };
        map_size -= offset;
        let trailing = map_size - MAX_SIZE;
        if trailing != 0 && unsafe { munmap(ptr.add(MAX_SIZE), trailing) } != 0 {
            panic!("munmap failed {}", std::io::Error::last_os_error());
        }
        if unsafe { madvise(ptr, MAX_SIZE, MADV_DONTDUMP) } != 0 {
            panic!(
                "madvise(MADV_DONTDUMP) failed {}",
                std::io::Error::last_os_error()
            );
        }
        self.num_regions += 1;
        ptr as *mut u8
    }

    fn deallocate_region(ptr: *mut u8) {
        if unsafe { munmap(ptr as *mut c_void, MAX_SIZE) } != 0 {
            panic!("munmap failed {}", std::io::Error::last_os_error());
        }
    }

    fn split_and_lock(
        &mut self,
        ptr: *mut u8,
        initial_order: usize,
        target_order: usize,
    ) -> *mut u8 {
        debug_assert!(target_order <= initial_order);
        if initial_order >= PAGE_ORDER {
            let size_to_lock = 1 << target_order.max(PAGE_ORDER);
            if unsafe { mlock(ptr as *const c_void, size_to_lock) } != 0 {
                panic!("mlock failed: {}", std::io::Error::last_os_error());
            }
        }
        for order in (target_order..initial_order).rev() {
            let _inserted = self
                .get_free_list(order)
                .insert(unsafe { ptr.offset(1 << order) });
            debug_assert!(_inserted);
        }
        ptr
    }

    fn compute_order_from_layout(layout: std::alloc::Layout) -> usize {
        if layout.align() > MAX_SIZE {
            panic!("cannot satisfy alignment greater than {MAX_SIZE}");
        }
        let size = layout
            .pad_to_align()
            .size()
            .next_power_of_two()
            .max(MIN_SIZE);
        debug_assert!(size.is_power_of_two());
        if size > MAX_SIZE {
            panic!("cannot allocate more than {MAX_SIZE} bytes at once");
        }
        std::mem::size_of_val(&size) * 8 - 1 - size.leading_zeros() as usize
    }

    pub fn alloc(&mut self, layout: std::alloc::Layout) -> NonNull<u8> {
        let order = Self::compute_order_from_layout(layout);
        let mut result = None;
        for current_order in order..=MAX_ORDER {
            let free_list = self.get_free_list(current_order);
            if let Some(&ptr) = free_list.iter().next() {
                let _present = free_list.remove(&ptr);
                debug_assert!(_present);
                result = Some((ptr, current_order));
                break;
            }
        }
        let result = result.unwrap_or_else(|| (self.allocate_new_region(), MAX_ORDER));
        NonNull::new(self.split_and_lock(result.0, result.1, order))
            .expect("should never return a null pointer")
    }

    fn unlock_pages_and_discard_content(ptr: *mut u8, order: usize) {
        debug_assert!(ptr as usize % PAGE_SIZE == 0);
        if unsafe { munlock(ptr as *const c_void, 1 << order) } != 0 {
            panic!("munlock failed: {}", std::io::Error::last_os_error());
        }
        if unsafe { madvise(ptr as *mut c_void, 1 << order, MADV_DONTNEED) } != 0 {
            panic!(
                "madvise(MADV_DONTNEED) failed {}",
                std::io::Error::last_os_error()
            );
        }
    }

    fn get_buddy(ptr: *mut u8, order: usize) -> *mut u8 {
        (ptr as usize ^ (1 << order)) as *mut u8
    }

    pub fn dealloc(&mut self, ptr: NonNull<u8>, layout: std::alloc::Layout) {
        let mut ptr = ptr.as_ptr();
        unsafe { std::slice::from_raw_parts_mut(ptr, layout.size()).zeroize() };
        let order = Self::compute_order_from_layout(layout);
        if order > PAGE_ORDER {
            Self::unlock_pages_and_discard_content(ptr, order);
        }
        let mut new_order = order;
        for current_order in order..MAX_ORDER {
            debug_assert!(ptr as usize % (1 << current_order) == 0);
            if current_order == PAGE_ORDER {
                Self::unlock_pages_and_discard_content(ptr, PAGE_ORDER);
            }
            let buddy = Self::get_buddy(ptr, current_order);
            if !self.get_free_list(current_order).remove(&buddy) {
                break;
            }
            ptr = (ptr as usize & !(1 << current_order)) as *mut u8;
            new_order = current_order + 1;
        }
        debug_assert!(ptr as usize % (1 << new_order) == 0);
        debug_assert!((MIN_ORDER..=MAX_ORDER).contains(&new_order));
        if new_order == MAX_ORDER && self.num_regions > 1 {
            Self::deallocate_region(ptr);
            self.num_regions -= 1;
            return;
        }
        let _inserted = self.get_free_list(new_order).insert(ptr);
        debug_assert!(_inserted);
    }
}

impl Default for SecretAllocator {
    fn default() -> Self {
        Self::new()
    }
}

impl Drop for SecretAllocator {
    fn drop(&mut self) {
        for ptr in self.get_free_list(MAX_ORDER).iter() {
            Self::deallocate_region(*ptr);
        }
    }
}

#[cfg(test)]
mod test {
    use std::alloc::Layout;

    use rand::Rng;

    use super::*;

    #[test]
    fn test1() {
        let mut a = SecretAllocator::new();
        let layout = Layout::from_size_align(MAX_SIZE, MAX_SIZE).unwrap();
        let ptr = a.alloc(layout);
        assert!(ptr.as_ptr() as usize % MAX_SIZE == 0);
        let slice = unsafe { std::slice::from_raw_parts_mut(ptr.as_ptr(), MAX_SIZE) };
        slice.fill(1);
        a.dealloc(ptr, layout);
        for order in MIN_ORDER..MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        assert!(a.get_free_list(MAX_ORDER).len() == 1);
        assert!(a.get_free_list(MAX_ORDER).contains(&ptr.as_ptr()));
        assert!(slice.iter().all(|&x| x == 0));
        assert_eq!(a.num_regions, 1);
    }

    #[test]
    fn test2() {
        let mut a = SecretAllocator::new();
        let layout = Layout::from_size_align(MIN_SIZE, MIN_SIZE).unwrap();
        let ptr = a.alloc(layout);
        assert!(ptr.as_ptr() as usize % MIN_SIZE == 0);
        let slice = unsafe { std::slice::from_raw_parts_mut(ptr.as_ptr(), MIN_SIZE) };
        slice.fill(1);

        assert!(
            a.get_free_list(MIN_ORDER).iter().next()
                == Some(&SecretAllocator::get_buddy(ptr.as_ptr(), MIN_ORDER))
        );
        for order in MIN_ORDER..MAX_ORDER {
            assert!(a.get_free_list(order).len() == 1);
        }
        assert!(a.get_free_list(MAX_ORDER).is_empty());

        a.dealloc(ptr, layout);
        for order in MIN_ORDER..MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        assert!(a.get_free_list(MAX_ORDER).len() == 1);
        assert!(a.get_free_list(MAX_ORDER).contains(&ptr.as_ptr()));
        assert!(slice.iter().all(|&x| x == 0));
        assert_eq!(a.num_regions, 1);
    }

    #[test]
    fn test3() {
        let mut a = SecretAllocator::new();
        let layout1 = Layout::from_size_align(MAX_SIZE, MAX_SIZE).unwrap();
        let ptr1 = a.alloc(layout1);
        assert!(ptr1.as_ptr() as usize % MAX_SIZE == 0);
        let layout2 = Layout::from_size_align(1, MAX_SIZE).unwrap();
        let ptr2 = a.alloc(layout2);
        assert!(ptr2.as_ptr() as usize % MAX_SIZE == 0);
        for order in MIN_ORDER..=MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        assert_eq!(a.num_regions, 2);
        a.dealloc(ptr1, layout1);
        a.dealloc(ptr2, layout2);
        assert!(a.get_free_list(MAX_ORDER).len() == 1);
        assert_eq!(a.num_regions, 1);
    }

    #[test]
    fn test4() {
        let mut a = SecretAllocator::new();
        let layout = Layout::from_size_align(0, 1).unwrap();
        let ptr = a.alloc(layout);
        assert!(a.get_free_list(MIN_ORDER).len() == 1);
        assert!(
            a.get_free_list(MIN_ORDER).iter().next()
                == Some(&SecretAllocator::get_buddy(ptr.as_ptr(), MIN_ORDER))
        );
        a.dealloc(ptr, layout);
    }

    #[test]
    fn test5() {
        let mut a = SecretAllocator::new();
        let size = (1 << (MAX_ORDER - 1)) - 1;
        let layout = Layout::from_size_align(size, 1).unwrap();
        let ptr1 = a.alloc(layout);
        let ptr2 = a.alloc(layout);
        for order in MIN_ORDER..=MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        a.dealloc(ptr2, layout);
        a.dealloc(ptr1, layout);
        for order in MIN_ORDER..MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        assert!(a.get_free_list(MAX_ORDER).len() == 1);
        assert!(a
            .get_free_list(MAX_ORDER)
            .contains(&ptr1.as_ptr().min(ptr2.as_ptr())));
        assert_eq!(a.num_regions, 1);
    }

    #[test]
    fn test6() {
        let mut a = SecretAllocator::new();
        let mut ptrs: Vec<_> = (0..MAX_ORDER)
            .map(|x| 1 << x)
            .map(|s| Layout::from_size_align(s, s).unwrap())
            .map(|layout| (a.alloc(layout), layout))
            .collect();
        ptrs.drain(..)
            .for_each(|(ptr, layout)| a.dealloc(ptr, layout));
        for order in MIN_ORDER..MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        assert!(a.get_free_list(MAX_ORDER).len() == 1);
        assert_eq!(a.num_regions, 1);
    }

    #[test]
    fn test7() {
        let mut a = SecretAllocator::new();
        let mut rng: rand::rngs::StdRng = rand::SeedableRng::seed_from_u64(12345);
        let mut ptrs: Vec<_> = (0..100)
            .map(|_| (rng.gen_range(1..MAX_SIZE), rng.gen_range(1..MAX_SIZE)))
            .map(|(size, align)| Layout::from_size_align(size, align.next_power_of_two()).unwrap())
            .map(|layout| (a.alloc(layout), layout))
            .collect();
        ptrs.drain(..)
            .for_each(|(ptr, layout)| a.dealloc(ptr, layout));
        for order in MIN_ORDER..MAX_ORDER {
            assert!(a.get_free_list(order).is_empty());
        }
        assert!(a.get_free_list(MAX_ORDER).len() == 1);
        assert_eq!(a.num_regions, 1);
    }
}
