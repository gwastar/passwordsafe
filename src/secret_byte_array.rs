use std::alloc::Layout;
use std::ptr::NonNull;
use std::rc::Rc;

use serde::{Deserialize, Serialize};
use zeroize::{Zeroize, Zeroizing};

use crate::crypto::crypto_memeq;
use crate::SECRET_ALLOCATOR;

struct SecretBytesInner {
    ptr: NonNull<u8>,
    size: usize,
}

impl SecretBytesInner {
    fn new(size: usize) -> Self {
        Self {
            ptr: SECRET_ALLOCATOR.with(|a| {
                a.borrow_mut()
                    .alloc(Layout::from_size_align(size, std::mem::align_of::<[u8; 1]>()).unwrap())
            }),
            size,
        }
    }

    fn as_slice(&self) -> &[u8] {
        unsafe { std::slice::from_raw_parts(self.ptr.as_ref(), self.size) }
    }

    fn as_mut_slice(&mut self) -> &mut [u8] {
        unsafe { std::slice::from_raw_parts_mut(self.ptr.as_ptr(), self.size) }
    }
}

impl Drop for SecretBytesInner {
    fn drop(&mut self) {
        SECRET_ALLOCATOR.with(|a| {
            a.borrow_mut().dealloc(
                self.ptr,
                Layout::from_size_align(self.size, std::mem::align_of::<[u8; 1]>()).unwrap(),
            )
        })
    }
}

pub struct SecretByteArray<const N: usize> {
    inner: Rc<SecretBytesInner>,
}

impl<const N: usize> SecretByteArray<N> {
    pub const SIZE: usize = N;

    fn new(bytes: &[u8; N]) -> Self {
        let mut inner = SecretBytesInner::new(N);
        inner.as_mut_slice().copy_from_slice(bytes);
        SecretByteArray {
            inner: Rc::new(inner),
        }
    }

    pub fn new_padded(bytes: &[u8]) -> Result<Self, (usize, usize)> {
        if bytes.len() > N {
            return Err((N, bytes.len()));
        }
        let mut result = Self::default();
        result.expose_secret_mut()[..bytes.len()].copy_from_slice(bytes);
        Ok(result)
    }

    pub fn expose_secret(&self) -> &[u8; N] {
        self.inner.as_slice().try_into().unwrap()
    }

    pub(crate) fn expose_secret_mut(&mut self) -> &mut [u8; N] {
        let inner: &mut SecretBytesInner =
            Rc::get_mut(&mut self.inner).expect("should only be called on unshared instances");
        inner.as_mut_slice().try_into().unwrap()
    }

    pub(crate) fn expose_secret_ptr(&self) -> *const u8 {
        self.expose_secret().as_ptr()
    }

    pub(crate) fn expose_secret_mut_ptr(&mut self) -> *mut u8 {
        self.expose_secret_mut().as_mut_ptr()
    }

    pub fn unpad_zeroes(&self) -> SecretBytes {
        let slice = self.inner.as_slice();
        let bytes = if let Some(pos) = slice.iter().rposition(|&b| b != 0) {
            slice.split_at(pos + 1).0
        } else {
            &[]
        };
        if bytes.len() == N {
            return SecretBytes {
                inner: Rc::clone(&self.inner),
            };
        }
        SecretBytes::new(bytes)
    }

    pub fn as_secret_bytes(&self) -> SecretBytes {
        SecretBytes {
            inner: Rc::clone(&self.inner),
        }
    }
}

impl<const N: usize> Default for SecretByteArray<N> {
    fn default() -> Self {
        let mut inner = SecretBytesInner::new(N);
        inner.as_mut_slice().fill(0);
        SecretByteArray {
            inner: Rc::new(inner),
        }
    }
}

impl<const N: usize> Clone for SecretByteArray<N> {
    fn clone(&self) -> Self {
        SecretByteArray {
            inner: Rc::clone(&self.inner),
        }
    }
}

impl<const N: usize> PartialEq for SecretByteArray<N> {
    fn eq(&self, other: &Self) -> bool {
        crypto_memeq(self.expose_secret(), other.expose_secret())
    }
}

impl<const N: usize> Eq for SecretByteArray<N> {}

impl<const N: usize> Serialize for SecretByteArray<N> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_bytes(self.expose_secret())
    }
}

struct SecretByteArrayVisitor<const N: usize>;

impl<'de, const N: usize> serde::de::Visitor<'de> for SecretByteArrayVisitor<N> {
    type Value = SecretByteArray<N>;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "an array of {N} bytes")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: serde::de::SeqAccess<'de>,
    {
        if let Some(size) = seq.size_hint() {
            if size != N {
                return Err(serde::de::Error::invalid_length(size, &self));
            }
        }
        let mut bytes = Zeroizing::new([0u8; N]);
        for (i, byte) in bytes.iter_mut().enumerate() {
            if let Some(b) = seq.next_element()? {
                *byte = b;
            } else {
                return Err(serde::de::Error::invalid_length(i + 1, &self));
            }
        }
        Ok(SecretByteArray::new(&bytes))
    }

    fn visit_byte_buf<E>(self, mut v: Vec<u8>) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        let result = if v.len() != N {
            Err(E::invalid_length(v.len(), &self))
        } else {
            Ok(SecretByteArray::new(v.as_slice().try_into().unwrap()))
        };
        v.zeroize();
        result
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        if v.len() != N {
            Err(E::invalid_length(v.len(), &self))
        } else {
            Ok(SecretByteArray::new(v.try_into().unwrap()))
        }
    }
}

impl<'de, const N: usize> Deserialize<'de> for SecretByteArray<N> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_byte_buf(SecretByteArrayVisitor)
    }
}

pub struct SecretBytes {
    inner: Rc<SecretBytesInner>,
}

impl SecretBytes {
    pub fn new_zeroed(size: usize) -> Self {
        let mut inner = SecretBytesInner::new(size);
        inner.as_mut_slice().fill(0);
        SecretBytes {
            inner: Rc::new(inner),
        }
    }

    pub fn new(bytes: &[u8]) -> Self {
        let mut inner = SecretBytesInner::new(bytes.len());
        inner.as_mut_slice().copy_from_slice(bytes);
        SecretBytes {
            inner: Rc::new(inner),
        }
    }

    pub fn expose_secret(&self) -> &[u8] {
        self.inner.as_slice()
    }

    // pub(crate) fn expose_secret_ptr(&self) -> *const u8 {
    //     self.bytes.as_ptr()
    // }

    // pub(crate) fn expose_secret_mut_ptr(&mut self) -> *mut u8 {
    //     Rc::get_mut(&mut self.bytes)
    //         .expect("should only be called on unshared instances")
    //         .as_mut_ptr()
    // }

    pub fn len(&self) -> usize {
        self.inner.size
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn pad_with_zeroes<const N: usize>(&self) -> Result<SecretByteArray<N>, (usize, usize)> {
        if self.len() > N {
            return Err((N, self.len()));
        }
        if self.len() == N {
            return Ok(SecretByteArray {
                inner: Rc::clone(&self.inner),
            });
        }
        let mut result = SecretByteArray::default();
        result.expose_secret_mut()[..self.len()].copy_from_slice(self.inner.as_slice());
        Ok(result)
    }
}

impl Clone for SecretBytes {
    fn clone(&self) -> Self {
        SecretBytes {
            inner: Rc::clone(&self.inner),
        }
    }
}

impl<const N: usize> From<SecretByteArray<N>> for SecretBytes {
    fn from(val: SecretByteArray<N>) -> Self {
        assert!(val.inner.size == N);
        SecretBytes { inner: val.inner }
    }
}

impl From<Vec<u8>> for SecretBytes {
    fn from(mut vec: Vec<u8>) -> Self {
        let result = SecretBytes::new(&vec);
        vec.zeroize();
        result
    }
}

impl From<String> for SecretBytes {
    fn from(mut str: String) -> Self {
        let result = SecretBytes::new(str.as_bytes());
        str.zeroize();
        result
    }
}

unsafe impl<const N: usize> Send for SecretByteArray<N> {}
unsafe impl Send for SecretBytes {}
