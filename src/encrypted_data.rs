use std::io::{Read, Write};

use libsodium_sys::{
    crypto_secretbox_MACBYTES, crypto_secretbox_NONCEBYTES, crypto_secretbox_detached,
    crypto_secretbox_open_detached,
};
use serde::{Deserialize, Serialize};
use zeroize::Zeroizing;

use crate::{
    crypto::{crypto_get_random_bytes, EncryptionKey},
    libsodium_call, ByteArray, SecretByteArray,
};

pub type Nonce = ByteArray<{ crypto_secretbox_NONCEBYTES as usize }>;
pub type Mac = ByteArray<{ crypto_secretbox_MACBYTES as usize }>;

#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct EncryptedData<const N: usize> {
    nonce: Nonce,
    mac: Mac,
    data: ByteArray<N>,
}

impl<const N: usize> EncryptedData<N> {
    pub const NONCE_SIZE: usize = Nonce::SIZE;
    pub const MAC_SIZE: usize = Mac::SIZE;
    pub const DATA_SIZE: usize = N;
    pub const SIZE_AS_BYTES: usize = Self::NONCE_SIZE + Self::MAC_SIZE + Self::DATA_SIZE;

    pub fn encrypt(data: &[u8; N], key: &EncryptionKey) -> Box<Self> {
        let mut result = Box::new(Self {
            nonce: crypto_get_random_bytes(),
            mac: Default::default(),
            data: Default::default(),
        });
        libsodium_call!(crypto_secretbox_detached(
            result.data.as_mut_ptr(),
            result.mac.as_mut_ptr(),
            data.as_ptr(),
            N as u64,
            result.nonce.as_ptr(),
            key.expose_secret_ptr()
        ))
        .get_result()
        .expect("crypto_secretbox_detached should always succeed");
        result
    }

    pub fn decrypt(self, key: &EncryptionKey) -> Result<SecretByteArray<N>, Self> {
        let mut buf = SecretByteArray::default();
        self.decrypt_internal(buf.expose_secret_mut_ptr(), key)
            .map(|_| buf)
    }

    pub fn decrypt_exposed(self, key: &EncryptionKey) -> Result<Box<[u8; N]>, Self> {
        let mut buf = Box::new([0u8; N]);
        self.decrypt_internal(buf.as_mut_ptr(), key).map(|_| buf)
    }

    fn decrypt_internal(self, out: *mut u8, key: &EncryptionKey) -> Result<(), Self> {
        if libsodium_call!(crypto_secretbox_open_detached(
            out,
            self.data.as_ptr(),
            self.mac.as_ptr(),
            N as u64,
            self.nonce.as_ptr(),
            key.expose_secret_ptr()
        ))
        .get_raw()
            == 0
        {
            Ok(())
        } else {
            Err(self)
        }
    }

    // TODO maybe use a binary serde serializer instead (look into CBOR)?
    pub fn serialize_into_bytes(&self) -> Box<[u8]> {
        let mut bytes = vec![0u8; Self::SIZE_AS_BYTES].into_boxed_slice();
        let (nonce, rest) = bytes.split_at_mut(Self::NONCE_SIZE);
        let (mac, data) = rest.split_at_mut(Self::MAC_SIZE);
        nonce.copy_from_slice(self.nonce.as_slice());
        mac.copy_from_slice(self.mac.as_slice());
        data.copy_from_slice(self.data.as_slice());
        bytes
    }

    // TODO maybe use a binary serde deserializer instead?
    pub fn deserialize_from_bytes(bytes: &[u8]) -> Self {
        // TODO would be nice to encode this in the type
        if bytes.len() != Self::SIZE_AS_BYTES {
            panic!("unexpected input length");
        }
        let (nonce, rest) = bytes.split_at(Self::NONCE_SIZE);
        let (mac, data) = rest.split_at(Self::MAC_SIZE);
        EncryptedData {
            nonce: ByteArray::new(nonce.try_into().unwrap()),
            mac: ByteArray::new(mac.try_into().unwrap()),
            data: ByteArray::new(data.try_into().unwrap()),
        }
    }
}

pub struct EncryptedWriter<'a> {
    // TODO maybe this should be SecretBytesArray but currently this is only used in the server
    // where it doesn't really matter
    buffer: Option<Zeroizing<Vec<u8>>>,
    key: EncryptionKey,
    writer: &'a mut dyn Write,
}

impl<'a> EncryptedWriter<'a> {
    pub fn new(writer: &'a mut dyn Write, key: EncryptionKey) -> Self {
        EncryptedWriter {
            buffer: Some(Zeroizing::new(Vec::new())),
            key,
            writer,
        }
    }
}

impl Drop for EncryptedWriter<'_> {
    fn drop(&mut self) {
        self.flush().ok();
    }
}

impl Write for EncryptedWriter<'_> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.buffer
            .as_mut()
            .ok_or_else(|| std::io::Error::from(std::io::ErrorKind::Other))?
            .extend_from_slice(buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        if let Some(mut buffer) = self.buffer.take() {
            let nonce: Nonce = crypto_get_random_bytes();
            let mut mac = Mac::default();
            libsodium_call!(crypto_secretbox_detached(
                buffer.as_mut_ptr(),
                mac.as_mut_ptr(),
                buffer.as_ptr(),
                buffer.len() as u64,
                nonce.as_ptr(),
                self.key.expose_secret_ptr()
            ))
            .get_result()
            .expect("crypto_secretbox_detached should always succeed");
            self.writer.write_all(nonce.as_slice())?;
            self.writer.write_all(mac.as_slice())?;
            self.writer.write_all(&buffer)?;
            self.writer.flush()
        } else {
            // already flushed
            Ok(())
        }
    }
}

pub struct EncryptedReader<'a> {
    // TODO maybe this should be SecretBytesArray but currently this is only used in the server
    buffer: Option<Zeroizing<Vec<u8>>>,
    index: usize,
    key: EncryptionKey,
    reader: &'a mut dyn Read,
}

impl<'a> EncryptedReader<'a> {
    pub fn new(reader: &'a mut dyn Read, key: EncryptionKey) -> Self {
        EncryptedReader {
            buffer: None,
            index: 0,
            key,
            reader,
        }
    }

    fn ensure_buffer(&mut self) -> std::io::Result<()> {
        if self.buffer.is_some() {
            return Ok(());
        }
        let mut buf = Zeroizing::new(Vec::new());
        self.reader.read_to_end(&mut buf)?;
        if buf.len() < Nonce::SIZE + Mac::SIZE {
            return Err(std::io::Error::from(std::io::ErrorKind::InvalidData));
        }
        let (nonce, rest) = buf.split_at_mut(Nonce::SIZE);
        let (mac, data) = rest.split_at_mut(Mac::SIZE);
        if libsodium_call!(crypto_secretbox_open_detached(
            data.as_mut_ptr(),
            data.as_ptr(),
            mac.as_ptr(),
            data.len() as u64,
            nonce.as_ptr(),
            self.key.expose_secret_ptr()
        ))
        .get_raw()
            != 0
        {
            return Err(std::io::Error::from(std::io::ErrorKind::InvalidData));
        }
        self.buffer = Some(buf);
        self.index += Nonce::SIZE + Mac::SIZE;
        Ok(())
    }
}

impl Read for EncryptedReader<'_> {
    fn read(&mut self, outbuf: &mut [u8]) -> std::io::Result<usize> {
        self.ensure_buffer()?;
        let buffer = self.buffer.as_ref().unwrap();
        if self.index >= buffer.len() {
            return Ok(0);
        }
        let rem = buffer.len() - self.index;
        let num_bytes = rem.min(outbuf.len());
        outbuf[..num_bytes].copy_from_slice(&buffer[self.index..self.index + num_bytes]);
        self.index += num_bytes;
        Ok(num_bytes)
    }
}

impl std::io::BufRead for EncryptedReader<'_> {
    fn consume(&mut self, amt: usize) {
        self.index += amt;
    }

    fn fill_buf(&mut self) -> std::io::Result<&[u8]> {
        self.ensure_buffer()?;
        let buffer = self.buffer.as_ref().unwrap();
        if self.index < buffer.len() {
            Ok(&buffer[self.index..])
        } else {
            Ok(&[])
        }
    }
}
