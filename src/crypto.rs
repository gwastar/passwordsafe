#![allow(unused_braces)] // workaround for apparent compiler bug

use std::{
    ffi::{c_int, c_void},
    ptr::null,
};

use libsodium_sys::{
    crypto_generichash, crypto_generichash_BYTES_MAX, crypto_generichash_BYTES_MIN,
    crypto_generichash_KEYBYTES_MAX, crypto_kdf_BYTES_MAX, crypto_kdf_BYTES_MIN,
    crypto_kdf_CONTEXTBYTES, crypto_kdf_KEYBYTES, crypto_kdf_derive_from_key, crypto_pwhash,
    crypto_pwhash_ALG_ARGON2ID13, crypto_pwhash_MEMLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_MIN,
    crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_OPSLIMIT_MAX, crypto_pwhash_OPSLIMIT_MIN,
    crypto_pwhash_SALTBYTES, crypto_pwhash_memlimit_max, crypto_secretbox_KEYBYTES,
    randombytes_buf, sodium_init, sodium_memcmp,
};
use serde::{Deserialize, Serialize};

use crate::{secret_byte_array::SecretBytes, ByteArray, Error, LoginKey, SecretByteArray};

pub type KdfKey = SecretByteArray<{ crypto_kdf_KEYBYTES as usize }>;
pub type EncryptionKey = SecretByteArray<{ crypto_secretbox_KEYBYTES as usize }>;

pub(crate) struct LibsodiumResult<T> {
    result: T,
    fn_name: &'static str,
}

impl<T> LibsodiumResult<T> {
    pub(crate) fn new(result: T, fn_name: &'static str) -> Self {
        LibsodiumResult { result, fn_name }
    }
}

impl LibsodiumResult<c_int> {
    pub(crate) fn get_raw(self) -> i32 {
        self.result
    }
    // TODO would be nice to implement the Try trait instead of doing this
    pub(crate) fn get_result(self) -> Result<i32, Error> {
        if self.result == 0 {
            Ok(self.result)
        } else {
            Err(Error::Libsodium(format!(
                "Libsodium function '{}' failed",
                self.fn_name
            )))
        }
    }
}

pub(crate) fn ensure_libsodium_initialized() {
    static SODIUM_INIT: std::sync::Once = std::sync::Once::new();
    SODIUM_INIT.call_once(|| {
        if unsafe { sodium_init() } < 0 {
            panic!("Failed to initialize libsodium");
        }
    });
}

#[macro_export]
macro_rules! libsodium_call {
    ( $f:ident ( $( $x:expr ),* )) => {{
	$crate::crypto::ensure_libsodium_initialized();
	let result = unsafe { $f($($x,)*) };
	$crate::crypto::LibsodiumResult::new(result, stringify!($f))
    }}
}

pub fn crypto_derive_key<const SUBKEY_LEN: usize>(
    subkey_id: u64,
    ctx: &[u8; crypto_kdf_CONTEXTBYTES as usize],
    key: &KdfKey,
) -> SecretByteArray<SUBKEY_LEN> {
    // TODO would be nice to encode this in the type
    if !(crypto_kdf_BYTES_MIN as usize..crypto_kdf_BYTES_MAX as usize).contains(&SUBKEY_LEN) {
        panic!("Invalid output length");
    }
    let mut subkey = SecretByteArray::default();
    libsodium_call!(crypto_kdf_derive_from_key(
        subkey.expose_secret_mut_ptr(),
        SUBKEY_LEN,
        subkey_id,
        ctx.as_ptr() as *const i8,
        key.expose_secret_ptr()
    ))
    .get_result()
    .expect("crypto_kdf_derive_from_key should always succeed");
    subkey
}

pub fn crypto_compute_secret_hash<const N: usize>(input: &[u8], key: Option<&[u8]>) -> SecretByteArray<N> {
    let mut result = SecretByteArray::<N>::default();
    crypto_compute_hash_internal(result.expose_secret_mut(), input, key);
    result
}

pub fn crypto_compute_hash<const N: usize>(input: &[u8], key: Option<&[u8]>) -> ByteArray<N> {
    let mut result = ByteArray::<N>::default();
    crypto_compute_hash_internal(result.as_mut_slice(), input, key);
    result
}

fn crypto_compute_hash_internal(output: &mut [u8], input: &[u8], key: Option<&[u8]>) {
    // TODO would be nice to encode this in the type
    if !(crypto_generichash_BYTES_MIN as usize..crypto_generichash_BYTES_MAX as usize).contains(&output.len())
    {
        panic!("Invalid output length");
    }
    let (key_ptr, key_len) = if let Some(key) = key {
        (key.as_ptr(), key.len())
    } else {
        (null(), 0)
    };
    if key_len > crypto_generichash_KEYBYTES_MAX as usize {
        panic!("Invalid key length");
    }
    libsodium_call!(crypto_generichash(
        output.as_mut_ptr(),
        output.len(),
        input.as_ptr(),
        input.len() as u64,
        key_ptr,
        key_len
    ))
    .get_result()
    .expect("crypto_generichash should always succeed");
}

#[must_use]
pub fn crypto_memeq<const N: usize>(a: &[u8; N], b: &[u8; N]) -> bool {
    libsodium_call!(sodium_memcmp(
        a.as_ptr() as *const c_void,
        b.as_ptr() as *const c_void,
        N
    ))
    .get_raw()
        == 0
}

pub fn crypto_get_random_bytes<const N: usize>() -> ByteArray<N> {
    let mut bytes = ByteArray::<N>::default();
    libsodium_call!(randombytes_buf(
        bytes.as_mut_ptr() as *mut c_void,
        bytes.len()
    ));
    bytes
}

pub fn crypto_get_secret_random_bytes<const N: usize>() -> SecretByteArray<N> {
    let mut bytes = SecretByteArray::default();
    libsodium_call!(randombytes_buf(
        bytes.expose_secret_mut_ptr() as *mut c_void,
        N
    ));
    bytes
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum PasswordHashArgs {
    ARGON2ID13 {
        opslimit: u64,
        memlimit: u64,
        salt: ByteArray<{ crypto_pwhash_SALTBYTES as usize }>,
    },
}

impl PasswordHashArgs {
    #[must_use]
    pub fn validate(&self) -> bool {
        match self {
            Self::ARGON2ID13 {
                opslimit,
                memlimit,
                salt: _,
            } => {
                if !(crypto_pwhash_OPSLIMIT_MIN as u64..crypto_pwhash_OPSLIMIT_MAX as u64)
                    .contains(opslimit)
                {
                    return false;
                }
                #[allow(non_snake_case)]
                let crypto_pwhash_MEMLIMIT_MAX = unsafe { crypto_pwhash_memlimit_max() };
                if !(crypto_pwhash_MEMLIMIT_MIN as u64..crypto_pwhash_MEMLIMIT_MAX as u64)
                    .contains(memlimit)
                {
                    return false;
                }
            }
        }
        true
    }

    fn hash(&self, out: &mut [u8], data: &[u8]) -> Result<(), Error> {
        match self {
            Self::ARGON2ID13 {
                salt,
                opslimit,
                memlimit,
            } => libsodium_call!(crypto_pwhash(
                out.as_mut_ptr(),
                out.len() as u64,
                data.as_ptr() as *const i8,
                data.len() as u64,
                salt.as_ptr(),
                *opslimit,
                *memlimit as usize,
                crypto_pwhash_ALG_ARGON2ID13 as i32
            ))
            .get_result()
            .map(|_| ()),
        }
    }

    pub fn hash_key<const HASH_LEN: usize>(
        &self,
        key: &LoginKey,
    ) -> Result<ByteArray<HASH_LEN>, Box<dyn std::error::Error>> {
        let mut hash = ByteArray::default();
        self.hash(hash.as_mut_slice(), key.expose_secret())?;
        Ok(hash)
    }

    pub fn hash_password<const HASH_LEN: usize>(
        &self,
        password: &SecretBytes,
    ) -> Result<SecretByteArray<HASH_LEN>, Box<dyn std::error::Error>> {
        let mut hash = SecretByteArray::default();
        self.hash(hash.expose_secret_mut(), password.expose_secret())?;
        Ok(hash)
    }
}

impl Default for PasswordHashArgs {
    fn default() -> Self {
        Self::ARGON2ID13 {
            opslimit: crypto_pwhash_OPSLIMIT_INTERACTIVE as u64,
            memlimit: crypto_pwhash_MEMLIMIT_INTERACTIVE as u64,
            salt: crypto_get_random_bytes(),
        }
    }
}
