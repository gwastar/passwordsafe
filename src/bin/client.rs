use std::str::FromStr;

use passwordsafe::{
    client_session::{Session, SessionError},
    fingerprint::{get_fingerprint_base64, get_fingerprint_randomart},
    secure_connection::PublicKey,
};
use rustyline::Editor;

struct TerminalSession {
    rl: Editor<()>,
    session: Session,
}

impl TerminalSession {
    fn new() -> Result<Self, Box<dyn std::error::Error>> {
        // TODO let the user provide addr as command line argument
        let mut rl = Editor::<()>::new()?;
        let addr = "127.0.0.1:5678";
        let addr = std::net::SocketAddr::from_str(addr)?;
        let mut verify_public_key =
            |public_key: &PublicKey| -> Result<(), Box<dyn std::error::Error>> {
                println!("Server public key: {}", get_fingerprint_base64(public_key));
                println!("{}", get_fingerprint_randomart(public_key));
                let reply = rl.readline("Proceed? [Y/n]")?;
                if reply.is_empty() || reply.eq_ignore_ascii_case("y") {
                    Ok(())
                } else {
                    Err(SessionError::PublicKeyVerificationFailure.into())
                }
            };
        let connection = Session::setup_connection(addr, &mut verify_public_key)?;

        let register = loop {
	    println!("Login or register? [LOGIN/register]");
            let action = rl.readline(">> ")?;
            if action.is_empty() || action.eq_ignore_ascii_case("login") {
                break false;
            } else if action.eq_ignore_ascii_case("register") {
                break true;
            }
        };

        let username = rl.readline("Username: ")?;
        let password = rl.readline("Password: ")?;

        // TODO let the user provide command line arguments to log in

        let session = if register {
            Session::register(connection, username, password.into())?
        } else {
            Session::login(connection, username, password.into())?
        };
        Ok(TerminalSession { rl, session })
    }

    fn run(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        loop {
            let line = self.rl.readline(">> ")?;
            let mut tokens = line.split_whitespace();
            let action = tokens.next();
            if action.is_none() {
                continue;
            }
            let action = action.unwrap();
            let mut get_arg = |prompt: &str| -> Result<String, Box<dyn std::error::Error>> {
                Ok(match tokens.next() {
                    Some(tok) => tok.to_string(),
                    None => self.rl.readline(prompt)?,
                })
            };

            if action.eq_ignore_ascii_case("get") {
                let id = get_arg("Id: ")?;
                match self.session.get_password(id.as_bytes())? {
                    Some(data) => println!("{}", std::str::from_utf8(data.expose_secret())?),
                    None => println!("Password not found"),
                }
            } else if action.eq_ignore_ascii_case("add") {
                let id = get_arg("Id: ")?;
                let password = get_arg("Password: ")?;
                match self.session.add_password(id.as_bytes(), password.into())? {
                    true => println!("Password added"),
                    false => println!("Password Id already exists"),
                }
            } else if action.eq_ignore_ascii_case("del") {
                let id = get_arg("Id: ")?;
                match self.session.delete_password(id.as_bytes())? {
                    true => println!("Password deleted"),
                    false => println!("Password not found"),
                }
            } else if action.eq_ignore_ascii_case("list") {
                let list = self.session.list_passwords()?;
                let mut list = list
                    .iter()
                    .map(|x| std::str::from_utf8(x))
                    .collect::<Result<Vec<_>, _>>()?;
                list.sort_unstable();
                for id in list {
                    println!("{id}");
                }
            } else if action.eq_ignore_ascii_case("change_master_password") {
                // TODO better command name
                let old_password = self.rl.readline("Old password: ")?;
                let new_password = self.rl.readline("New password: ")?;
                if self
                    .session
                    .change_master_password(old_password.into(), new_password.into())?
                {
                    println!("Master password changed");
                } else {
                    println!("Failed to change master password");
                }
            } else if action.eq_ignore_ascii_case("delete_account") {
                let password = self.rl.readline("Password: ")?;
                if self.session.delete_account(password.into())? {
                    println!("Account deleted");
                    break Ok(());
                } else {
                    println!("Failed to delete account");
                }
            } else if action.eq_ignore_ascii_case("quit") {
                break Ok(());
	    } else if action.eq_ignore_ascii_case("help") {
		println!("get [id] -- get password with 'id'");
		println!("add [id] -- add new password with 'id'");
		println!("del [id] -- delete password with 'id'");
		println!("list     -- list password ids");
		println!("change_master_password");
		println!("delete_account");
		println!("quit");
            } else {
                println!("Unknown command, type help to list all commands");
            }
        }
    }
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    TerminalSession::new()?.run()
}

fn main() {
    if let Err(err) = run() {
        println!("{err}");
    }
}
