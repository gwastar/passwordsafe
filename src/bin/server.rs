#![allow(unused_braces)] // workaround for apparent compiler bug

use base64::{engine::general_purpose, Engine as _};
use std::{
    cell::RefCell,
    fmt::Debug,
    fs::{DirBuilder, File},
    io::{ErrorKind, Read, Write},
    num::NonZeroUsize,
    os::unix::{fs::DirBuilderExt, prelude::OsStrExt},
    rc::Rc,
    sync::Arc,
    time::SystemTime,
};

use openat::*;
use passwordsafe::{
    crypto::{crypto_compute_secret_hash, crypto_memeq, EncryptionKey, PasswordHashArgs},
    encrypted_data::{EncryptedReader, EncryptedWriter},
    fingerprint::{get_fingerprint_base64, get_fingerprint_randomart},
    secure_connection::{KeyGenerationSeed, PublicKey, SecretKey},
    *,
};
use serde::{Deserialize, Serialize};
use threadpool::ThreadPool;

const DEFAULT_DATA_DIRECTORY_NAME: &str = "userdata";

struct Cleanup<F: FnMut()> {
    closure: Option<F>,
}

impl<F: FnMut()> Cleanup<F> {
    fn new(closure: F) -> Self {
        Cleanup {
            closure: Some(closure),
        }
    }

    fn clear(&mut self) {
        self.closure = None;
    }
}

impl<F: FnMut()> Drop for Cleanup<F> {
    fn drop(&mut self) {
        if let Some(ref mut closure) = self.closure {
            closure();
        }
    }
}

#[derive(Serialize, Deserialize)]
struct UserData {
    user_pwhash_args: PasswordHashArgs,
    server_pwhash_args: PasswordHashArgs,
    login_key_hash: ByteArray<{ LoginKey::SIZE }>,
    encrypted_master_key: EncryptedMasterKey,
}

struct UserSession {
    username: Username,
    connection: Rc<RefCell<SecureConnection>>,
    user_dir: Dir,
    passwords_dir: Dir,
    data: Box<UserData>,
    master_key: EncryptionKey,
}

impl Debug for UserSession {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "User '{}'", self.username)
    }
}

impl UserSession {
    fn bytes_to_filename(bytes: &[u8]) -> String {
        general_purpose::URL_SAFE_NO_PAD.encode(bytes)
    }

    fn filename_to_bytes(filename: &[u8]) -> Result<Box<[u8]>, base64::DecodeError> {
	general_purpose::URL_SAFE_NO_PAD.decode(filename).map(Vec::into_boxed_slice)
    }

    fn login(
        connection: Rc<RefCell<SecureConnection>>,
        username: Username,
        master_key: EncryptionKey,
    ) -> Result<Option<Self>, Box<dyn std::error::Error>> {
        if !(MIN_USERNAME_LENGTH..MAX_USERNAME_LENGTH).contains(&username.len()) {
            return Err(Error::InvalidArgument.into());
        }
        let dirname = Self::bytes_to_filename(username.as_bytes());
        let user_dir = match Dir::open(dirname) {
            Ok(dir) => dir,
            Err(error) if error.kind() == ErrorKind::NotFound => {
                connection
                    .borrow_mut()
                    .send_msg(&Message::WrongUsernameOrPassword)?;
                return Ok(None);
            }
            Err(error) => return Err(error.into()),
        };
        let mut data_file = match user_dir.open_file("data") {
            Ok(file) => file,
            Err(error) if error.kind() == ErrorKind::NotFound => {
                // user is being registered but data file wasn't created yet
                // (or data file somehow got lost)
                connection
                    .borrow_mut()
                    .send_msg(&Message::WrongUsernameOrPassword)?;
                return Ok(None);
            }
            Err(error) => return Err(error.into()),
        };
        let data: Box<UserData> = serde_json::from_reader(&mut EncryptedReader::new(
            &mut data_file,
            master_key.clone(),
        ))?;
        connection
            .borrow_mut()
            .send_msg(&Message::PasswordHashArgs(Box::new(data.user_pwhash_args)))?;
        let login_key = match connection.borrow_mut().receive_msg()? {
            Message::LoginKey { key } => key,
            msg => return Err(Error::UnexpectedMessage(msg.to_string()).into()),
        };
        let login_key_hash = data.server_pwhash_args.hash_key(&login_key)?;
        if !crypto_memeq(&login_key_hash, &data.login_key_hash) {
            connection
                .borrow_mut()
                .send_msg(&Message::WrongUsernameOrPassword)?;
            return Ok(None);
        }
        connection
            .borrow_mut()
            .send_msg(&Message::EncryptedMasterKey(Box::new(
                data.encrypted_master_key,
            )))?;
        Ok(Some(UserSession {
            connection,
            data,
            passwords_dir: user_dir.sub_dir("passwords")?,
            user_dir,
            username,
            master_key,
        }))
    }

    fn create_user_data(
        login_key: &LoginKey,
        encrypted_master_key: &EncryptedMasterKey,
        user_pwhash_args: &PasswordHashArgs,
    ) -> Result<Box<UserData>, Box<dyn std::error::Error>> {
        let server_pwhash_args = PasswordHashArgs::default();
        let login_key_hash = server_pwhash_args.hash_key(login_key)?;
        Ok(Box::new(UserData {
            server_pwhash_args,
            user_pwhash_args: *user_pwhash_args,
            login_key_hash,
            encrypted_master_key: *encrypted_master_key,
        }))
    }

    fn write_user_data_file(
        data: &UserData,
        data_file: &mut File,
        key: &EncryptionKey,
    ) -> Result<(), Box<dyn std::error::Error>> {
        data_file.set_len(0)?;
        {
            let mut writer = EncryptedWriter::new(data_file, key.clone());
            serde_json::to_writer(&mut writer, &data)?;
            writer.flush()?;
        }
        data_file.sync_all()?;
        Ok(())
    }

    fn register(
        connection: Rc<RefCell<SecureConnection>>,
        username: Username,
        master_key: EncryptionKey,
    ) -> Result<Option<Self>, Box<dyn std::error::Error>> {
        if !(MIN_USERNAME_LENGTH..MAX_USERNAME_LENGTH).contains(&username.len()) {
            return Err(Error::InvalidArgument.into());
        }
        let dirname = Self::bytes_to_filename(username.as_bytes());
        match DirBuilder::new().mode(0o700).create(&dirname) {
            Ok(()) => {}
            Err(error) if error.kind() == ErrorKind::AlreadyExists => {
                connection
                    .borrow_mut()
                    .send_msg(&Message::UsernameAlreadyExists)?;
                return Ok(None);
            }
            Err(error) => return Err(error.into()),
        }
        let mut cleanup = Cleanup::new(|| {
            if std::fs::remove_dir_all(&dirname).is_err() {
                eprintln!("failed to clean up directory '{}'", &dirname);
            }
        });
        let user_dir = Dir::open(&dirname)?;
        user_dir.create_dir("passwords", 0o700)?;
        let passwords_dir = user_dir.sub_dir("passwords")?;
        let mut data_file = user_dir.new_unnamed_file(0o600)?;
        connection.borrow_mut().send_msg(&Message::Success)?;

        let (login_key, encrypted_master_key, user_pwhash_args) =
            match connection.borrow_mut().receive_msg()? {
                Message::Register2 {
                    login_key,
                    encrypted_master_key,
                    pwhash_args,
                } => (login_key, encrypted_master_key, pwhash_args),
                msg => return Err(Error::UnexpectedMessage(msg.to_string()).into()),
            };
        if !user_pwhash_args.validate() {
            return Err(Error::InvalidArgument.into());
        }

        let data = Self::create_user_data(&login_key, &encrypted_master_key, &user_pwhash_args)?;

        Self::write_user_data_file(&data, &mut data_file, &master_key)?;
        user_dir.link_file_at(&data_file, "data")?;

        connection.borrow_mut().send_msg(&Message::Success)?;
        cleanup.clear();
        Ok(Some(UserSession {
            connection,
            data,
            passwords_dir,
            user_dir,
            username,
            master_key,
        }))
    }

    pub fn new(
        connection: Rc<RefCell<SecureConnection>>,
        master_key: EncryptionKey,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        loop {
            let msg = connection.borrow_mut().receive_msg()?;
            let session = match msg {
                Message::Login { username } => {
                    Self::login(Rc::clone(&connection), username, master_key.clone())?
                }
                Message::Register { username } => {
                    Self::register(Rc::clone(&connection), username, master_key.clone())?
                }
                msg => return Err(Error::UnexpectedMessage(msg.to_string()).into()),
            };
            if let Some(session) = session {
                return Ok(session);
            }
        }
    }

    pub fn add_password(
        &mut self,
        id: &EncryptedPasswordId,
        password: &EncryptedPasswordData,
    ) -> Result<Message, Box<dyn std::error::Error>> {
        let mut file = self.passwords_dir.new_unnamed_file(0o600)?;
        {
            let mut writer = EncryptedWriter::new(&mut file, self.master_key.clone());
            writer.write_all(&password.serialize_into_bytes())?;
            writer.flush()?;
        }
        file.sync_all()?;
        match self
            .passwords_dir
            .link_file_at(&file, Self::bytes_to_filename(&id.serialize_into_bytes()))
        {
            Ok(()) => Ok(Message::Success),
            Err(error) if error.kind() == ErrorKind::AlreadyExists => {
                Ok(Message::PasswordAlreadyExists)
            }
            Err(error) => Err(error.into()),
        }
    }

    pub fn get_password(
        &mut self,
        id: &EncryptedPasswordId,
    ) -> Result<Message, Box<dyn std::error::Error>> {
        match self
            .passwords_dir
            .open_file(Self::bytes_to_filename(&id.serialize_into_bytes()))
        {
            Ok(mut file) => {
                let mut reader = EncryptedReader::new(&mut file, self.master_key.clone());
                let mut buf = Vec::new();
                reader.read_to_end(&mut buf)?;
                Ok(Message::Password {
                    data: Box::new(EncryptedPasswordData::deserialize_from_bytes(&buf)),
                })
            }
            Err(error) if error.kind() == ErrorKind::NotFound => Ok(Message::PasswordNotFound),
            Err(error) => Err(error.into()),
        }
    }

    pub fn delete_password(
        &mut self,
        id: &EncryptedPasswordId,
    ) -> Result<Message, Box<dyn std::error::Error>> {
        match self
            .passwords_dir
            .remove_file(Self::bytes_to_filename(&id.serialize_into_bytes()))
        {
            Ok(()) => Ok(Message::Success),
            Err(error) if error.kind() == ErrorKind::NotFound => Ok(Message::PasswordNotFound),
            Err(error) => Err(error.into()),
        }
    }

    pub fn list_passwords(&mut self) -> Result<Message, Box<dyn std::error::Error>> {
        let mut ids = Vec::new();
        for file in self.passwords_dir.list_dir(".")? {
            let bytes = Self::filename_to_bytes(file?.file_name().as_bytes())?;
            if bytes.len() != EncryptedPasswordId::SIZE_AS_BYTES {
                return Err(Error::FileCorruption.into());
            }
            ids.push(EncryptedPasswordId::deserialize_from_bytes(&bytes));
        }
        Ok(Message::PasswordList { ids })
    }

    fn create_unique_file(dir: &Dir) -> Result<(File, String), Box<dyn std::error::Error>> {
        loop {
            let rand = rand::random::<u64>();
            let filename = format!("tmp{rand}");
            match dir.new_file(&filename, 0o600) {
                Ok(file) => return Ok((file, filename)),
                Err(error) if error.kind() == ErrorKind::AlreadyExists => {}
                Err(error) => return Err(error.into()),
            };
        }
    }

    pub fn change_master_password(
        &mut self,
        old_key: &LoginKey,
        new_key: &LoginKey,
        pwhash_args: &PasswordHashArgs,
        encrypted_master_key: &EncryptedMasterKey,
    ) -> Result<Message, Box<dyn std::error::Error>> {
        let login_key_hash = self.data.server_pwhash_args.hash_key(old_key)?;
        if !crypto_memeq(&login_key_hash, &self.data.login_key_hash) {
            return Ok(Message::WrongUsernameOrPassword);
        }

        let data = Self::create_user_data(new_key, encrypted_master_key, pwhash_args)?;

        let (mut data_file, data_filename) = Self::create_unique_file(&self.user_dir)?;
        Self::write_user_data_file(&data, &mut data_file, &self.master_key)?;
        self.user_dir.local_rename(data_filename, "data")?;

        Ok(Message::Success)
    }

    pub fn delete_account(
        &mut self,
        key: &LoginKey,
    ) -> Result<Message, Box<dyn std::error::Error>> {
        let login_key_hash = self.data.server_pwhash_args.hash_key(key)?;
        if !crypto_memeq(&login_key_hash, &self.data.login_key_hash) {
            return Ok(Message::WrongUsernameOrPassword);
        }
        let dirname = Self::bytes_to_filename(self.username.as_bytes());
        let temp_name = format!(
            "deleted-{}-{}",
            dirname,
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)?
                .as_nanos()
        );
        std::fs::rename(&dirname, &temp_name)?;
        std::fs::remove_dir_all(temp_name)?;
        Ok(Message::Success)
    }

    pub fn handle_messages(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        loop {
            let msg = self.connection.borrow_mut().receive_msg()?;
            let response = match msg {
                Message::AddPassword { id, password } => self.add_password(&id, &password)?,
                Message::GetPassword { id } => self.get_password(&id)?,
                Message::DeletePassword { id } => self.delete_password(&id)?,
                Message::ListPasswords => self.list_passwords()?,
                Message::ChangeMasterPassword {
                    old_key,
                    new_key,
                    pwhash_args,
                    encrypted_master_key,
                } => self.change_master_password(
                    &old_key,
                    &new_key,
                    &pwhash_args,
                    &encrypted_master_key,
                )?,
                Message::DeleteAccount { key } => {
                    let response = self.delete_account(&key)?;
                    self.connection.borrow_mut().send_msg(&response)?;
                    return Ok(());
                }
                Message::Quit => return Ok(()),
                msg => return Err(Error::UnexpectedMessage(msg.to_string()).into()),
            };
            self.connection.borrow_mut().send_msg(&response)?;
        }
    }
}

fn handle_connection(
    connection: Rc<RefCell<SecureConnection>>,
    master_key: EncryptionKey,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut session = UserSession::new(connection, master_key)?;
    session.handle_messages()
}

fn handle_client(
    client: std::net::TcpStream,
    master_key: EncryptionKey,
    public_key: &PublicKey,
    secret_key: SecretKey,
) -> Result<(), Box<dyn std::error::Error>> {
    let connection = Rc::new(RefCell::new(SecureConnection::new_server(
        client,
        public_key,
        &secret_key,
    )?));
    drop(secret_key);
    match handle_connection(Rc::clone(&connection), master_key) {
        Ok(()) => Ok(()),
        Err(err) => {
            let (err, result) = match err.downcast() {
                Ok(err) => (*err, Ok(())),
                Err(err) => (Error::SystemError, Err(err)),
            };
            connection.borrow_mut().send_msg(&Message::Error(err))?;
            result
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // TODO use actual arg parser
    let mut file = if let Some(path) = std::env::args().nth(1) {
        File::open(path)?
    } else {
        eprintln!("Provide master key file");
        return Ok(());
    };

    let mut buf = Vec::with_capacity(128);
    file.read_to_end(&mut buf)?;
    let min_size = EncryptionKey::SIZE.max(KeyGenerationSeed::SIZE);
    if buf.len() < min_size {
        eprintln!(
            "Master key file too short (min: {}, got: {})",
            min_size,
            buf.len()
        );
        return Ok(());
    }
    let seed = crypto_compute_secret_hash(&buf, Some(b"seed"));
    let master_key = crypto_compute_secret_hash(&buf, Some(b"master_key"));
    let (public_key, secret_key) = SecureConnection::generate_keys(&seed);
    let public_key = Arc::new(public_key);
    drop(seed);

    println!("Public key: {}", get_fingerprint_base64(&public_key));
    println!("{}", get_fingerprint_randomart(&public_key));

    let data_dir_path = if let Some(path) = std::env::args().nth(2) {
        path
    } else {
        DEFAULT_DATA_DIRECTORY_NAME.into()
    };
    std::env::set_current_dir(data_dir_path)?;

    let listener = std::net::TcpListener::bind("127.0.0.1:5678")?;
    let thread_count = std::thread::available_parallelism().map_or(2, NonZeroUsize::get);
    let threadpool = ThreadPool::new(thread_count);
    for client in listener.incoming().flatten() {
        let (public_key, secret_key, master_key) = (
            Arc::clone(&public_key),
            secret_key.clone(),
            master_key.clone(),
        );
        threadpool.execute(move || {
            if let Err(error) = handle_client(client, master_key, &public_key, secret_key) {
                eprintln!("{error:?}");
            }
        });
    }
    threadpool.join();
    Ok(())
}
