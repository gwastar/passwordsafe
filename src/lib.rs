pub mod byte_array;
pub mod client_session;
pub mod crypto;
pub mod encrypted_data;
pub mod fingerprint;
pub mod secret_allocator;
pub mod secret_byte_array;
pub mod secure_connection;

use std::{
    cell::RefCell,
    fmt::{Debug, Display},
};

use crypto::{EncryptionKey, PasswordHashArgs};
use derive_more::Display;
use secret_allocator::SecretAllocator;
use serde::{Deserialize, Serialize};
use strum_macros::IntoStaticStr;

pub use byte_array::{ByteArray, PaddedByteArray};
pub use crypto::KdfKey;
pub use encrypted_data::EncryptedData;
pub use secret_byte_array::SecretByteArray;
pub use secure_connection::SecureConnection;

#[derive(Debug, Serialize, Deserialize, Display, Clone)]
pub enum Error {
    #[display(fmt = "Received unexpected message: {_0}")]
    UnexpectedMessage(String),
    #[display(fmt = "Invalid argument")]
    InvalidArgument,
    #[display(fmt = "Libsodium function '{_0}' failed")]
    Libsodium(String),
    #[display(fmt = "Unknown message tag: {_0}")]
    UnknownMessageTag(u8),
    #[display(fmt = "Message too long")]
    MessageTooLong,
    #[display(fmt = "File corruption")]
    FileCorruption,
    #[display(fmt = "System error")]
    SystemError,
}

thread_local! {
    pub static SECRET_ALLOCATOR: RefCell<SecretAllocator> = RefCell::new(SecretAllocator::new());
}

impl std::error::Error for Error {}

pub const MIN_USERNAME_LENGTH: usize = 3;
pub const MAX_USERNAME_LENGTH: usize = 64;
pub type Username = String; // TODO make a special type for this to limit the length and ensure minimum length
pub type LoginKey = KdfKey;
pub const PASSWORD_ID_SIZE: usize = 64;
pub type EncryptedPasswordId = EncryptedData<PASSWORD_ID_SIZE>;
pub const PASSWORD_DATA_SIZE: usize = 4096;
pub type EncryptedPasswordData = EncryptedData<PASSWORD_DATA_SIZE>;
pub type EncryptedMasterKey = EncryptedData<{ EncryptionKey::SIZE }>;

#[derive(Serialize, Deserialize, IntoStaticStr)]
pub enum Message {
    Login {
        username: Username,
    },
    LoginKey {
        key: LoginKey,
    },
    Register {
        username: Username,
    },
    Register2 {
        login_key: LoginKey,
        pwhash_args: Box<PasswordHashArgs>,
        encrypted_master_key: Box<EncryptedMasterKey>,
    },
    AddPassword {
        id: Box<EncryptedPasswordId>,
        password: Box<EncryptedPasswordData>,
    },
    GetPassword {
        id: Box<EncryptedPasswordId>,
    },
    DeletePassword {
        id: Box<EncryptedPasswordId>,
    },
    ListPasswords,
    ChangeMasterPassword {
        old_key: LoginKey,
        new_key: LoginKey,
        pwhash_args: Box<PasswordHashArgs>,
        encrypted_master_key: Box<EncryptedMasterKey>,
    },
    DeleteAccount {
        key: LoginKey,
    },

    Success,
    PasswordHashArgs(Box<PasswordHashArgs>),
    EncryptedMasterKey(Box<EncryptedMasterKey>),
    UsernameAlreadyExists,
    WrongUsernameOrPassword,
    Password {
        data: Box<EncryptedPasswordData>,
    },
    PasswordNotFound,
    PasswordAlreadyExists,
    PasswordList {
        ids: Vec<EncryptedPasswordId>, // TODO this should probably contain metadata about each password
    },
    Error(Error),

    Quit,
}

impl Display for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.into())
    }
}

impl Message {
    pub fn create_login(username: Username) -> Self {
        Message::Login { username }
    }

    pub fn create_register(username: Username) -> Self {
        Message::Register { username }
    }

    pub fn create_register2(
        login_key: LoginKey,
        encrypted_master_key: Box<EncryptedMasterKey>,
        pwhash_args: Box<PasswordHashArgs>,
    ) -> Self {
        Message::Register2 {
            login_key,
            encrypted_master_key,
            pwhash_args,
        }
    }

    pub fn create_add_password(
        id: Box<EncryptedPasswordId>,
        password: Box<EncryptedPasswordData>,
    ) -> Self {
        Message::AddPassword { id, password }
    }

    pub fn create_get_password(id: Box<EncryptedPasswordId>) -> Self {
        Message::GetPassword { id }
    }

    pub fn create_password(data: Box<EncryptedPasswordData>) -> Self {
        Message::Password { data }
    }

    pub fn create_delete_password(id: Box<EncryptedPasswordId>) -> Self {
        Message::DeletePassword { id }
    }

    pub fn create_change_master_password(
        old_key: LoginKey,
        new_key: LoginKey,
        pwhash_args: Box<PasswordHashArgs>,
        encrypted_master_key: Box<EncryptedMasterKey>,
    ) -> Self {
        Message::ChangeMasterPassword {
            old_key,
            new_key,
            encrypted_master_key,
            pwhash_args,
        }
    }
}
