use crate::secure_connection::PublicKey;
use base64::{engine::general_purpose, Engine as _};

pub fn get_fingerprint_base64(pk: &PublicKey) -> String {
    general_purpose::STANDARD_NO_PAD.encode(pk.as_ref())
}

pub fn get_fingerprint_randomart(pk: &PublicKey) -> String {
    const CHARS: &[u8] = b" .o+=*BOX@%&#/^SE";
    const LEN: u8 = CHARS.len() as u8 - 1;
    const HEIGHT: usize = 9;
    const WIDTH: usize = 17;

    let mut field = [[0u8; WIDTH]; HEIGHT];
    let mut y = HEIGHT as isize / 2;
    let mut x = WIDTH as isize / 2;
    for &(mut b) in pk.as_ref() {
        for _ in 0..4 {
            if (b & 1) != 0 {
                x += 1
            } else {
                x -= 1
            };
            if (b & 2) != 0 {
                y += 1
            } else {
                y -= 1
            };
            x = x.clamp(0, WIDTH as isize - 1);
            y = y.clamp(0, HEIGHT as isize - 1);
            let f = &mut field[y as usize][x as usize];
            if *f < LEN - 2 {
                *f += 1;
            }
            b >>= 2;
        }
    }
    field[HEIGHT / 2][WIDTH / 2] = LEN - 1;
    field[y as usize][x as usize] = LEN;
    let mut s = String::new();
    let horizontal_border = |s: &mut String| {
        s.push('+');
        s.extend(std::iter::repeat('-').take(WIDTH));
        s.push('+');
        s.push('\n');
    };
    horizontal_border(&mut s);
    for row in field {
        s.push('|');
        for v in row {
            s.push(CHARS[v as usize] as char);
        }
        s.push('|');
        s.push('\n');
    }
    horizontal_border(&mut s);
    s
}
