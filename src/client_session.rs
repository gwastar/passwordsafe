use std::{collections::HashMap, fmt::Debug, net::SocketAddr};

use derive_more::Display;

use crate::{
    crypto::{crypto_derive_key, crypto_get_secret_random_bytes, EncryptionKey, PasswordHashArgs},
    secret_byte_array::SecretBytes,
    secure_connection::PublicKey,
    EncryptedData, EncryptedMasterKey, EncryptedPasswordId, Error, KdfKey, LoginKey, Message,
    PaddedByteArray, SecureConnection, Username, MAX_USERNAME_LENGTH, MIN_USERNAME_LENGTH,
};

#[derive(Debug, Display)]
pub enum SessionError {
    #[display(fmt = "Unknown username")]
    UnknownUsername,
    #[display(fmt = "Wrong password")]
    WrongPassword,
    #[display(fmt = "Username already exists")]
    UsernameAlreadyExists,
    #[display(fmt = "Received bogus data from server")]
    BogusReply,
    #[display(fmt = "Password is too weak")]
    WeakPassword,
    #[display(fmt = "Input too long (max: {_0} bytes, got: {_1} bytes)")]
    InputTooLong(usize, usize),
    #[display(fmt = "Input too short (min: {_0} bytes, got: {_1} bytes)")]
    InputTooShort(usize, usize),
    #[display(fmt = "Received unexpected message from server: {_0}")]
    UnexpectedMessage(String),
    #[display(fmt = "Public key verification failed")]
    PublicKeyVerificationFailure,
    #[display(fmt = "Server returned error: {_0}")]
    ServerError(Error),
    #[display(fmt = "IO error: {_0}")]
    IOError(std::io::Error),
    #[display(fmt = "System error: {_0}")]
    SystemError(Box<dyn std::error::Error>),
}

impl std::error::Error for SessionError {}

impl From<std::io::Error> for SessionError {
    fn from(error: std::io::Error) -> Self {
        Self::IOError(error)
    }
}

impl From<Box<dyn std::error::Error>> for SessionError {
    fn from(error: Box<dyn std::error::Error>) -> Self {
        match error.downcast() {
            Ok(error) => Self::IOError(*error),
            Err(error) => Self::SystemError(error),
        }
    }
}

macro_rules! match_server_response {
    ( $connection:expr, { $( $pat:pat => $expr:expr ),* } ) => {
	(|| -> Result<_, SessionError> {
	    match $connection.receive_msg()? {
		$( $pat => $expr ),*,
		Message::Error(error) => Err(SessionError::ServerError(error)),
		msg => Err(SessionError::UnexpectedMessage(msg.to_string()))
	    }
	})()
    }
}

#[repr(u64)]
enum SubkeyId {
    LoginKey,
    MasterKeyKey,
}

pub struct Session {
    connection: SecureConnection,
    master_key: EncryptionKey,
    passwords: HashMap<Box<[u8]>, Box<EncryptedPasswordId>>,
    pwhash_args: Box<PasswordHashArgs>,
}

impl Session {
    const MIN_PASSWORD_LENGTH: usize = 6;
    const KEY_DERIVATION_CTX: &'static [u8; 8] = b"PWDSAFE_";

    #[allow(clippy::type_complexity)]
    pub fn setup_connection(
        addr: SocketAddr,
        verify_public_key: &mut dyn FnMut(&PublicKey) -> Result<(), Box<dyn std::error::Error>>,
    ) -> Result<SecureConnection, SessionError> {
        let socket =
            std::net::TcpStream::connect_timeout(&addr, std::time::Duration::from_secs(10))?;
        let conn = SecureConnection::new_client(socket, verify_public_key)?;
        Ok(conn)
    }

    fn get_pwhash_and_login_key(
        password: &SecretBytes,
        pwhash_args: &PasswordHashArgs,
    ) -> Result<(KdfKey, LoginKey), SessionError> {
        if !pwhash_args.validate() {
            return Err(SessionError::BogusReply);
        }
        let pwhash = pwhash_args.hash_password(password)?;

        let login_key =
            crypto_derive_key(SubkeyId::LoginKey as u64, Self::KEY_DERIVATION_CTX, &pwhash);

        Ok((pwhash, login_key))
    }

    fn check_username(username: &Username) -> Result<(), SessionError> {
        if username.len() < MIN_USERNAME_LENGTH {
            return Err(SessionError::InputTooShort(
                MIN_USERNAME_LENGTH,
                username.len(),
            ));
        }
        if username.len() > MAX_USERNAME_LENGTH {
            return Err(SessionError::InputTooLong(
                MAX_USERNAME_LENGTH,
                username.len(),
            ));
        }
        Ok(())
    }

    fn check_password(password: &SecretBytes) -> Result<(), SessionError> {
        if password.len() < Self::MIN_PASSWORD_LENGTH {
            // TODO use zxcvbn for password strength estimation
            return Err(SessionError::WeakPassword);
        }
        Ok(())
    }

    pub fn login(
        mut connection: SecureConnection,
        username: Username,
        password: SecretBytes,
    ) -> Result<Self, SessionError> {
        Self::check_username(&username)?;
        Self::check_password(&password)?;

        connection.send_msg(&Message::create_login(username))?;
        let pwhash_args = match_server_response!(connection, {
            Message::PasswordHashArgs(args) => Ok(args),
            Message::WrongUsernameOrPassword => Err(SessionError::UnknownUsername)
        })?;

        let (pwhash, login_key) = Self::get_pwhash_and_login_key(&password, &pwhash_args)?;

        connection.send_msg(&Message::LoginKey { key: login_key })?;
        let master_key = match_server_response!(connection, {
            Message::EncryptedMasterKey(master_key) => Ok(master_key),
            Message::WrongUsernameOrPassword => Err(SessionError::WrongPassword)
        })?;

        let master_key_key = crypto_derive_key(
            SubkeyId::MasterKeyKey as u64,
            Self::KEY_DERIVATION_CTX,
            &pwhash,
        );

        let master_key = master_key
            .decrypt(&master_key_key)
            .map_err(|_| SessionError::BogusReply)?;

        connection.send_msg(&Message::ListPasswords)?;
        let encrypted_ids = match_server_response!(connection, {
            Message::PasswordList { ids } => Ok(ids)
        })?;
        let mut passwords = HashMap::new();
        for ref encrypted_id in encrypted_ids {
            let id = Self::decrypt_and_unpad(encrypted_id, &master_key)?;
            passwords.insert(id, Box::new(*encrypted_id));
        }

        Ok(Session {
            master_key,
            connection,
            passwords,
            pwhash_args,
        })
    }

    fn setup_new_password(
        password: &SecretBytes,
        master_key: &EncryptionKey,
    ) -> Result<(Box<PasswordHashArgs>, LoginKey, Box<EncryptedMasterKey>), SessionError> {
        let pwhash_args = Box::<PasswordHashArgs>::default();
        let pwhash = pwhash_args.hash_password(password)?;

        let login_key =
            crypto_derive_key(SubkeyId::LoginKey as u64, Self::KEY_DERIVATION_CTX, &pwhash);

        let master_key_key = crypto_derive_key(
            SubkeyId::MasterKeyKey as u64,
            Self::KEY_DERIVATION_CTX,
            &pwhash,
        );

        let encrypted_master_key =
            EncryptedMasterKey::encrypt(master_key.expose_secret(), &master_key_key);

        Ok((pwhash_args, login_key, encrypted_master_key))
    }

    pub fn register(
        mut connection: SecureConnection,
        username: Username,
        password: SecretBytes,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        Self::check_username(&username)?;
        Self::check_password(&password)?;

        connection.send_msg(&Message::create_register(username))?;
        match_server_response!(connection, {
            Message::Success => Ok(()),
            Message::UsernameAlreadyExists => Err(SessionError::UsernameAlreadyExists)
        })?;

        let master_key = crypto_get_secret_random_bytes();

        let (pwhash_args, login_key, encrypted_master_key) =
            Self::setup_new_password(&password, &master_key)?;

        connection.send_msg(&Message::create_register2(
            login_key,
            encrypted_master_key,
            pwhash_args.clone(),
        ))?;

        match_server_response!(connection, {
            Message::Success => Ok(())
        })?;

        Ok(Session {
            master_key,
            connection,
            passwords: HashMap::new(),
            pwhash_args,
        })
    }

    fn pad_and_encrypt_secret<const N: usize>(
        data: &SecretBytes,
        key: &EncryptionKey,
    ) -> Result<Box<EncryptedData<N>>, SessionError> {
        let padded_data = data
            .pad_with_zeroes()
            .map_err(|(max, got)| SessionError::InputTooLong(max, got))?;
        Ok(EncryptedData::encrypt(padded_data.expose_secret(), key))
    }

    fn decrypt_and_unpad_secret<const N: usize>(
        encrypted_data: &EncryptedData<N>,
        key: &EncryptionKey,
    ) -> Result<SecretBytes, SessionError> {
        let decrypted_data = encrypted_data
            .decrypt(key)
            .map_err(|_| SessionError::BogusReply)?;
        Ok(decrypted_data.unpad_zeroes())
    }

    fn pad_and_encrypt<const N: usize>(
        data: &[u8],
        key: &EncryptionKey,
    ) -> Result<Box<EncryptedData<N>>, SessionError> {
        let padded_data = PaddedByteArray::construct(data)
            .map_err(|(max, got)| SessionError::InputTooLong(max, got))?;
        Ok(EncryptedData::encrypt(
            padded_data.bytes_with_padding(),
            key,
        ))
    }

    fn decrypt_and_unpad<const N: usize>(
        encrypted_data: &EncryptedData<N>,
        key: &EncryptionKey,
    ) -> Result<Box<[u8]>, SessionError> {
        let padded_id: Box<PaddedByteArray<N>> = encrypted_data
            .decrypt_exposed(key)
            .map_err(|_| SessionError::BogusReply)?
            .into();
        Ok(padded_id.bytes_without_padding().into())
    }

    pub fn get_password(&mut self, id: &[u8]) -> Result<Option<SecretBytes>, SessionError> {
        if let Some(encrypted_id) = self.passwords.get(id) {
            self.connection
                .send_msg(&Message::create_get_password(encrypted_id.clone()))?;
            let encrypted_data = match_server_response!(self.connection, {
            Message::Password { data } => Ok(Some(data)),
            Message::PasswordNotFound => Ok(None)
                })?;
            if let Some(encrypted_data) = encrypted_data {
                Ok(Some(Self::decrypt_and_unpad_secret(
                    &encrypted_data,
                    &self.master_key,
                )?))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    pub fn add_password(
        &mut self,
        id: &[u8],
        password_data: SecretBytes,
    ) -> Result<bool, SessionError> {
        if self.passwords.contains_key(id) {
            return Ok(false);
        }
        let encrypted_id = Self::pad_and_encrypt(id, &self.master_key)?;
        let encrypted_password_data =
            Self::pad_and_encrypt_secret(&password_data, &self.master_key)?;
        self.connection.send_msg(&Message::create_add_password(
            encrypted_id.clone(),
            encrypted_password_data,
        ))?;
        match_server_response!(self.connection, {
            Message::Success => {
        self.passwords.insert(id.into(), encrypted_id);
        Ok(true)
        },
            Message::PasswordAlreadyExists => Ok(false)
        })
    }

    pub fn delete_password(&mut self, id: &[u8]) -> Result<bool, SessionError> {
        if let Some(encrypted_id) = self.passwords.get(id) {
            self.connection
                .send_msg(&Message::create_delete_password(encrypted_id.clone()))?;
            match_server_response!(self.connection, {
            Message::Success => {
                        self.passwords.remove(id);
                        Ok(true)
            },
            Message::PasswordNotFound => Ok(false)
            })
        } else {
            Ok(false)
        }
    }

    pub fn list_passwords(&mut self) -> Result<Vec<Box<[u8]>>, SessionError> {
        Ok(self.passwords.keys().cloned().collect())
    }

    pub fn change_master_password(
        &mut self,
        old_password: SecretBytes,
        new_password: SecretBytes,
    ) -> Result<bool, SessionError> {
        let (_, old_key) = Self::get_pwhash_and_login_key(&old_password, &self.pwhash_args)?;

        let (pwhash_args, new_key, encrypted_master_key) =
            Self::setup_new_password(&new_password, &self.master_key)?;

        self.connection
            .send_msg(&Message::create_change_master_password(
                old_key,
                new_key,
                pwhash_args.clone(),
                encrypted_master_key,
            ))?;
        match_server_response!(self.connection, {
            Message::Success => {
        self.pwhash_args = pwhash_args;
        Ok(true)
        },
            Message::WrongUsernameOrPassword => Ok(false)
        })
    }

    pub fn delete_account(&mut self, password: SecretBytes) -> Result<bool, SessionError> {
        let (_, key) = Self::get_pwhash_and_login_key(&password, &self.pwhash_args)?;

        self.connection.send_msg(&Message::DeleteAccount { key })?;
        match_server_response!(self.connection, {
            Message::Success => Ok(true),
            Message::WrongUsernameOrPassword => Ok(false)
        })
    }
}

impl Debug for Session {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Session")
            .field("", &self.passwords.keys())
            .finish_non_exhaustive()
    }
}
