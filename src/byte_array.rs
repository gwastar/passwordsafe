use std::ops::{Deref, DerefMut};

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ByteArray<const N: usize> {
    bytes: [u8; N],
}

impl<const N: usize> ByteArray<N> {
    pub const SIZE: usize = N;

    pub const fn new(bytes: [u8; N]) -> Self {
        ByteArray { bytes }
    }

    pub fn construct<T>(input: T) -> Box<Self>
    where
        T: Into<Box<[u8; N]>>,
    {
        input.into().into()
    }

    pub fn as_ptr(&self) -> *const u8 {
        self.bytes.as_ptr()
    }

    pub fn as_mut_ptr(&mut self) -> *mut u8 {
        self.bytes.as_mut_ptr()
    }
}

impl<const N: usize> Default for ByteArray<N> {
    fn default() -> Self {
        ByteArray { bytes: [0u8; N] }
    }
}

impl<const N: usize> Deref for ByteArray<N> {
    type Target = [u8; N];
    fn deref(&self) -> &Self::Target {
        &self.bytes
    }
}

impl<const N: usize> DerefMut for ByteArray<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.bytes
    }
}

impl<const N: usize> AsRef<[u8; N]> for ByteArray<N> {
    fn as_ref(&self) -> &[u8; N] {
        self
    }
}

impl<const N: usize> From<Box<PaddedByteArray<N>>> for Box<ByteArray<N>> {
    fn from(bytes: Box<PaddedByteArray<N>>) -> Self {
        assert!(std::mem::size_of::<PaddedByteArray<N>>() == std::mem::size_of::<ByteArray<N>>());
        assert!(std::mem::align_of::<PaddedByteArray<N>>() == std::mem::align_of::<ByteArray<N>>());
        unsafe { Box::from_raw(Box::into_raw(bytes) as *mut ByteArray<N>) }
    }
}

impl<const N: usize> From<Box<[u8; N]>> for Box<ByteArray<N>> {
    fn from(bytes: Box<[u8; N]>) -> Self {
        assert!(std::mem::size_of::<[u8; N]>() == std::mem::size_of::<ByteArray<N>>());
        assert!(std::mem::align_of::<[u8; N]>() == std::mem::align_of::<ByteArray<N>>());
        unsafe { Box::from_raw(Box::into_raw(bytes) as *mut ByteArray<N>) }
    }
}

impl<const N: usize> From<[u8; N]> for ByteArray<N> {
    fn from(bytes: [u8; N]) -> Self {
        ByteArray::new(bytes)
    }
}

impl<const N: usize> Serialize for ByteArray<N> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_bytes(&self[..])
    }
}

struct ByteArrayVisitor<const N: usize>;

impl<'de, const N: usize> serde::de::Visitor<'de> for ByteArrayVisitor<N> {
    type Value = ByteArray<N>;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "an array of {N} bytes")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: serde::de::SeqAccess<'de>,
    {
        if let Some(size) = seq.size_hint() {
            if size != N {
                return Err(serde::de::Error::invalid_length(size, &self));
            }
        }
        let mut bytes = [0u8; N];
        for (i, byte) in bytes.iter_mut().enumerate() {
            if let Some(b) = seq.next_element()? {
                *byte = b;
            } else {
                return Err(serde::de::Error::invalid_length(i + 1, &self));
            }
        }
        Ok(ByteArray::new(bytes))
    }

    fn visit_byte_buf<E>(self, v: Vec<u8>) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        if v.len() != N {
            Err(E::invalid_length(v.len(), &self))
        } else {
            Ok(ByteArray::new(v.try_into().unwrap()))
        }
    }

    fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        if v.len() != N {
            Err(E::invalid_length(v.len(), &self))
        } else {
            Ok(ByteArray::new(v.try_into().unwrap()))
        }
    }
}

impl<'de, const N: usize> Deserialize<'de> for ByteArray<N> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_byte_buf(ByteArrayVisitor)
    }
}

#[derive(Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub struct PaddedByteArray<const N: usize> {
    bytes: ByteArray<N>,
}

impl<const N: usize> PaddedByteArray<N> {
    pub const SIZE: usize = N;

    pub fn new<T>(bytes: T) -> Result<Self, (usize, usize)>
    where
        T: AsRef<[u8]>,
    {
        let input = bytes.as_ref();
        if input.len() > N {
            return Err((N, input.len()));
        }
        let mut result = Self::default();
        result.bytes[..input.len()].copy_from_slice(input);
        Ok(result)
    }

    pub fn construct<T>(input: T) -> Result<Box<Self>, (usize, usize)>
    where
        T: Into<Vec<u8>>,
    {
        let mut vec = input.into();
        if vec.len() > N {
            return Err((N, vec.len()));
        }
        vec.resize(N, 0);
        let array: Box<[u8; N]> = vec.into_boxed_slice().try_into().unwrap();
        Ok(Into::<Box<ByteArray<N>>>::into(array).into())
    }

    pub fn bytes_without_padding(&self) -> &[u8] {
        if let Some(pos) = self.bytes.iter().rposition(|&b| b != 0) {
            self.bytes.split_at(pos + 1).0
        } else {
            &[]
        }
    }

    pub fn bytes_with_padding(&self) -> &[u8; N] {
        &self.bytes
    }
}

impl<const N: usize> From<Box<ByteArray<N>>> for Box<PaddedByteArray<N>> {
    fn from(bytes: Box<ByteArray<N>>) -> Self {
        assert!(std::mem::size_of::<ByteArray<N>>() == std::mem::size_of::<PaddedByteArray<N>>());
        assert!(std::mem::align_of::<ByteArray<N>>() == std::mem::align_of::<PaddedByteArray<N>>());
        unsafe { Box::from_raw(Box::into_raw(bytes) as *mut PaddedByteArray<N>) }
    }
}

impl<const N: usize> From<[u8; N]> for PaddedByteArray<N> {
    fn from(bytes: [u8; N]) -> Self {
        PaddedByteArray {
            bytes: bytes.into(),
        }
    }
}

impl<const N: usize> From<ByteArray<N>> for PaddedByteArray<N> {
    fn from(bytes: ByteArray<N>) -> Self {
        PaddedByteArray { bytes }
    }
}

impl<const N: usize> From<&[u8; N]> for PaddedByteArray<N> {
    fn from(bytes: &[u8; N]) -> Self {
        PaddedByteArray {
            bytes: (*bytes).into(),
        }
    }
}

impl<const N: usize> From<Box<[u8; N]>> for Box<PaddedByteArray<N>> {
    fn from(bytes: Box<[u8; N]>) -> Self {
        assert!(std::mem::size_of::<[u8; N]>() == std::mem::size_of::<ByteArray<N>>());
        assert!(std::mem::align_of::<[u8; N]>() == std::mem::align_of::<ByteArray<N>>());
        unsafe { Box::from_raw(Box::into_raw(bytes) as *mut PaddedByteArray<N>) }
    }
}

impl<const N: usize> TryFrom<&[u8]> for PaddedByteArray<N> {
    type Error = (usize, usize);
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
	Self::new(bytes)
    }
}
