use std::{
    io::{Read, Write},
    ptr::{null, null_mut},
};

use libsodium_sys::{
    crypto_kx_PUBLICKEYBYTES, crypto_kx_SECRETKEYBYTES, crypto_kx_SEEDBYTES,
    crypto_kx_SESSIONKEYBYTES, crypto_kx_client_session_keys, crypto_kx_keypair,
    crypto_kx_seed_keypair, crypto_kx_server_session_keys,
    crypto_secretstream_xchacha20poly1305_ABYTES,
    crypto_secretstream_xchacha20poly1305_HEADERBYTES,
    crypto_secretstream_xchacha20poly1305_TAG_FINAL,
    crypto_secretstream_xchacha20poly1305_TAG_MESSAGE,
    crypto_secretstream_xchacha20poly1305_TAG_PUSH,
    crypto_secretstream_xchacha20poly1305_TAG_REKEY,
    crypto_secretstream_xchacha20poly1305_init_pull,
    crypto_secretstream_xchacha20poly1305_init_push, crypto_secretstream_xchacha20poly1305_pull,
    crypto_secretstream_xchacha20poly1305_push, crypto_secretstream_xchacha20poly1305_state,
};
use zeroize::Zeroize;

use crate::{libsodium_call, ByteArray, Error, Message, SecretByteArray};

#[repr(u8)]
enum MessageTag {
    Message = crypto_secretstream_xchacha20poly1305_TAG_MESSAGE as u8,
    Push = crypto_secretstream_xchacha20poly1305_TAG_PUSH as u8,
    Rekey = crypto_secretstream_xchacha20poly1305_TAG_REKEY as u8,
    Final = crypto_secretstream_xchacha20poly1305_TAG_FINAL as u8,
}

impl TryFrom<u8> for MessageTag {
    type Error = Error;
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        use MessageTag::*;
        match value {
            x if x == Message as u8 => Ok(Message),
            x if x == Push as u8 => Ok(Push),
            x if x == Rekey as u8 => Ok(Rekey),
            x if x == Final as u8 => Ok(Final),
            x => Err(Error::UnknownMessageTag(x)),
        }
    }
}

pub struct SecureConnection {
    socket: std::net::TcpStream,
    crypto_state:
        SecretByteArray<{ 2 * std::mem::size_of::<crypto_secretstream_xchacha20poly1305_state>() }>,
}

pub type KeyGenerationSeed = SecretByteArray<{ crypto_kx_SEEDBYTES as usize }>;
pub type PublicKey = ByteArray<{ crypto_kx_PUBLICKEYBYTES as usize }>;
pub type SecretKey = SecretByteArray<{ crypto_kx_SECRETKEYBYTES as usize }>;

impl SecureConnection {
    const MAX_MSG_SIZE: u32 = 16384;

    fn get_tx_state(&mut self) -> *mut crypto_secretstream_xchacha20poly1305_state {
        self.crypto_state.expose_secret_mut_ptr() as *mut _
    }

    fn get_rx_state(&mut self) -> *mut crypto_secretstream_xchacha20poly1305_state {
        unsafe { self.get_tx_state().add(1) }
    }

    pub fn generate_keys(seed: &KeyGenerationSeed) -> (PublicKey, SecretKey) {
        let mut pk = PublicKey::default();
        let mut sk = SecretKey::default();
        libsodium_call!(crypto_kx_seed_keypair(
            pk.as_mut_ptr(),
            sk.expose_secret_mut_ptr(),
            seed.expose_secret_ptr()
        ))
        .get_result()
        .expect("crypto_kx_seed_keypair should always succeed");
        (pk, sk)
    }

    #[allow(clippy::type_complexity)]
    pub fn new_client(
        socket: std::net::TcpStream,
        verify_public_key: &mut dyn FnMut(&PublicKey) -> Result<(), Box<dyn std::error::Error>>,
    ) -> Result<SecureConnection, Box<dyn std::error::Error>> {
        let mut pk = PublicKey::default();
        let mut sk = SecretKey::default();
        libsodium_call!(crypto_kx_keypair(
            pk.as_mut_ptr(),
            sk.expose_secret_mut_ptr()
        ))
        .get_result()?;
        Self::new(socket, verify_public_key, false, &pk, &sk)
    }

    pub fn new_server(
        socket: std::net::TcpStream,
        pk: &PublicKey,
        sk: &SecretKey,
    ) -> Result<SecureConnection, Box<dyn std::error::Error>> {
        Self::new(socket, &mut |_| Ok(()), true, pk, sk)
    }

    #[allow(clippy::type_complexity)]
    pub fn new(
        mut socket: std::net::TcpStream,
        verify_public_key: &mut dyn FnMut(&PublicKey) -> Result<(), Box<dyn std::error::Error>>,
        is_server: bool,
        pk: &PublicKey,
        sk: &SecretKey,
    ) -> Result<SecureConnection, Box<dyn std::error::Error>> {
        socket.write_all(pk.as_slice())?;
        let mut other_pk = PublicKey::default();
        socket.read_exact(other_pk.as_mut_slice())?;
        verify_public_key(&other_pk)?;
        let mut rx = SecretByteArray::<{ crypto_kx_SESSIONKEYBYTES as usize }>::default();
        let mut tx = SecretByteArray::<{ crypto_kx_SESSIONKEYBYTES as usize }>::default();
        if is_server {
            libsodium_call!(crypto_kx_server_session_keys(
                rx.expose_secret_mut_ptr(),
                tx.expose_secret_mut_ptr(),
                pk.as_ptr(),
                sk.expose_secret_ptr(),
                other_pk.as_mut_ptr()
            ))
            .get_result()?;
        } else {
            libsodium_call!(crypto_kx_client_session_keys(
                rx.expose_secret_mut_ptr(),
                tx.expose_secret_mut_ptr(),
                pk.as_ptr(),
                sk.expose_secret_ptr(),
                other_pk.as_mut_ptr()
            ))
            .get_result()?;
        }

        let mut conn = SecureConnection {
            socket,
            crypto_state: Default::default(),
        };

        let mut tx_header = [0u8; crypto_secretstream_xchacha20poly1305_HEADERBYTES as usize];
        libsodium_call!(crypto_secretstream_xchacha20poly1305_init_push(
            conn.get_tx_state(),
            tx_header.as_mut_ptr(),
            tx.expose_secret_ptr()
        ))
        .get_result()?;
        conn.socket.write_all(&tx_header)?;

        let mut rx_header = [0u8; crypto_secretstream_xchacha20poly1305_HEADERBYTES as usize];
        conn.socket.read_exact(&mut rx_header)?;
        libsodium_call!(crypto_secretstream_xchacha20poly1305_init_pull(
            conn.get_rx_state(),
            rx_header.as_mut_ptr(),
            rx.expose_secret_ptr()
        ))
        .get_result()?;
        Ok(conn)
    }

    fn send_bytes(
        &mut self,
        buf: &[u8],
        tag: MessageTag,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let cbuf_size = buf.len() + crypto_secretstream_xchacha20poly1305_ABYTES as usize;
        let mut cbuf = Vec::with_capacity(cbuf_size);
        libsodium_call!(crypto_secretstream_xchacha20poly1305_push(
            self.get_tx_state(),
            cbuf.as_mut_ptr(),
            null_mut::<u64>(),
            buf.as_ptr(),
            buf.len() as u64,
            null::<u8>(),
            0,
            tag as u8
        ))
        .get_result()?;
        unsafe { cbuf.set_len(cbuf_size) };
        self.socket.write_all(&cbuf)?;
        Ok(())
    }

    fn receive_bytes(&mut self, buf: &mut [u8]) -> Result<MessageTag, Box<dyn std::error::Error>> {
        let mut cbuf = vec![0u8; buf.len() + crypto_secretstream_xchacha20poly1305_ABYTES as usize];
        self.socket.read_exact(&mut cbuf)?;
        let mut tag = 0u8;
        libsodium_call!(crypto_secretstream_xchacha20poly1305_pull(
            self.get_rx_state(),
            buf.as_mut_ptr(),
            null_mut::<u64>(),
            &mut tag as *mut u8,
            cbuf.as_mut_ptr(),
            cbuf.len() as u64,
            null::<u8>(),
            0
        ))
        .get_result()?;
        Ok(MessageTag::try_from(tag)?)
    }

    fn send_msg_internal(
        &mut self,
        msg: &Message,
        tag: MessageTag,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut buf = serde_json::to_vec(msg)?; // TODO use SecretBytes? (implement SecretBytesWriter?)
        let size = buf.len();
        self.send_bytes(
            &[
                size as u8,
                (size >> 8) as u8,
                (size >> 16) as u8,
                (size >> 24) as u8,
            ],
            MessageTag::Message,
        )?;
        self.send_bytes(&buf, tag)?;
        buf.zeroize();
        Ok(())
    }

    pub fn send_msg(&mut self, msg: &Message) -> Result<(), Box<dyn std::error::Error>> {
        self.send_msg_internal(msg, MessageTag::Message)
    }

    pub fn receive_msg(&mut self) -> Result<Message, Box<dyn std::error::Error>> {
        let mut size_buf = [0u8; 4];
        self.receive_bytes(&mut size_buf)?;
        let size = size_buf[0] as u32
            | (size_buf[1] as u32) << 8
            | (size_buf[2] as u32) << 16
            | (size_buf[3] as u32) << 24;
        if size > Self::MAX_MSG_SIZE {
            return Err(Error::MessageTooLong.into());
        }
        let mut buf = vec![0u8; size as usize]; // TODO use SecretBytes?
        self.receive_bytes(&mut buf)?;
        let msg = serde_json::from_slice(&buf)?;
        buf.zeroize();
        Ok(msg)
    }
}

impl Drop for SecureConnection {
    fn drop(&mut self) {
        self.send_msg_internal(&Message::Quit, MessageTag::Final)
            .ok();
    }
}
